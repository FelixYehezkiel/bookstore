package com.example.demo.service;

import com.example.demo.entity.Product;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository repository;

    public Product saveProduct(Product product) {
        return repository.save(product);
    }
    public List<Product> getProducts() {
        return repository.findAll();
    }
    public String deleteProduct(int id) {
        repository.deleteById(id);
        return "Product removed!!";
    }
}
