package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dom4j.Text;

import javax.persistence.*;
import java.awt.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "PRODUCT_MASTER")
public class Product {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String author;
    private String publisher;
    private String yearofissue;
    private String description;
    @Column(columnDefinition="TEXT")
    private String img;
    private String price;

}
