import React from 'react';
import {BrowserRouter as Router,Route} from 'react-router-dom'
import Home from "./App"
import Add from "./Add"

function Main(){
    return (
    <Router>
        <Route exact path="/" component={Home} />
        <Route exact path="/add" component={Add} />
    </Router>
    )
}

export default Main;