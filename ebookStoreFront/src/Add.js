import React, { Component } from 'react';
import {Link} from 'react-router-dom'


export default class indexClient extends Component{


    state={
        name :'',
        author:'',
        publisher:'',
        yearofissue:'',
        price:'',
        img:'',
        description:''
    }



    handleChange = e => {
        this.setState({
            [e.target.name] : e.target.value});

    };

    handleSubmit = tambah =>{
        tambah.preventDefault();

        if ( 
            this.state.name === "" || 
            this.state.author === "" || 
            this.state.publisher === "" || 
            this.state.yearofissue === "" || 
            this.state.price === "" || 
            this.state.img === "" || 
            this.state.description === ""
            ) {  
            alert("Data tidak boleh kosong!");
        } 
        else {
                const bookPost = {
                    name : this.state.name,
                    author : this.state.author,
                    publisher : this.state.publisher,
                    yearofissue : this.state.yearofissue,
                    description : this.state.description,
                    price : this.state.price,
                    img : this.state.img
                };

                console.log(bookPost);
                fetch(`http://localhost:9090/add`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(bookPost)
                })
                .then(res => {
                    console.log(res)
                        alert("Permintaan berhasil terkirim!")
                        window.location.href = "http://localhost:3000"
                      })
                .catch(console.error);
            }
      };




render() {
    return (
        
        <div>
            <div className="container">
                <div className="leftBox">
                    <div className="namaToko">
                    <Link to="/">
        <h2>
          F-EbookStore
        </h2>
        </Link>
                    </div>

                    <div className="input">


                        <div className="formControl">
                            <div className="leftFormTitle"> <p>Judul Buku</p> </div>
                            <div className="leftFormControl"> <input type="text" name="name" onChange={this.handleChange} className="inputForm"/> </div>
                        </div>
                        

                        <div className="formControl">
                            <div className="leftFormTitle"> <p>Deskripsi</p> </div>
                            <div className="leftFormControl"> <input type="text" name="description" onChange={this.handleChange} className="inputForm"/> </div>
                        </div>


                        <div className="formControl">
                            <div className="leftFormTitle"> <p>Penulis</p> </div>
                            <div className="leftFormControl"> <input type="text" name="author" onChange={this.handleChange} className="inputForm"/> </div>
                        </div>


                        <div className="formControl">
                            <div className="leftFormTitle"> <p>Penerbit</p> </div>
                            <div className="leftFormControl"> <input type="text" name="publisher" onChange={this.handleChange} className="inputForm"/> </div>
                        </div>


                        <div className="formControl">
                            <div className="leftFormTitle"> <p>Tahun Terbit</p> </div>
                            <div className="leftFormControl"> <input type="text" name="yearofissue" onChange={this.handleChange} className="inputForm"/> </div>
                        </div>


                        <div className="formControl">
                            <div className="leftFormTitle"> <p>Harga Buku</p> </div>
                            <div className="leftFormControl"> <input type="text" name="price" onChange={this.handleChange} className="inputForm"/> </div>
                        </div>


                        <div className="formControl">
                            <div className="leftFormTitle"> <p>URL Gambar</p> </div>
                            <div className="leftFormControl"> <textarea type="text" name="img" onChange={this.handleChange} className="inputForm"/> </div>
                        </div>

                        </div>
                        <div className="buttonStyle">
                            <button type="submit" onClick={this.handleSubmit}>Submit</button>
                        </div>

                    

                    </div>
                </div>  
            </div>
   
        )
    }
}