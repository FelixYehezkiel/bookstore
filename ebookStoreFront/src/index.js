import React from 'react'
import ReactDOM from 'react-dom'
// import App from './App'
import Route from './Route'
ReactDOM.render(
  <React.StrictMode>
    <Route />
  </React.StrictMode>,
  document.getElementById('root')
)
