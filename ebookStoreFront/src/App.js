import React, { useState, useEffect } from 'react'
import axios from 'axios'
import CharacterGrid from './components/characters/CharacterGrid'
// import {Modal, Button} from 'react-bootstrap'
// import 'bootstrap/dist/css/bootstrap.min.css'
import {Link} from "react-router-dom"
import './App.css'


const App = () => {
  
  
  const [items, setItems] = useState([])
  const [isLoading, setIsLoading] = useState(true)
  const [query] = useState('')

  useEffect(() => {
    const fetchItems = async () => {
      setIsLoading(true)
      const result = await axios(
        `http://localhost:9090/books`
      )

      setItems(result.data)
      setIsLoading(false)
    }

    fetchItems()
  }, [query])



  return (
    <div className='container'>
      <div className='namaToko'>
        <h2>
          F-EbookStore
        </h2>
      </div>
      <div className='buttonPosition'>
      <Link to="./Add"><button>Tambah buku baru</button></Link>
      </div>
      <CharacterGrid isLoading={isLoading} items={items} />

    </div>
  )
  }
export default App
