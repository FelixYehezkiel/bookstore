import React from 'react'




const CharacterItem = ({ item }) => {

  function deleteData(){
    let del = window.confirm("Yakin ingin menghapus?")
    if (del) {
    fetch(`http://localhost:9090/delete/${item.id}`, {
      method: 'DELETE',
  })
  .then(res => {        
        alert("Berhasil")
        window.location.reload()
   })
  .catch(console.error);
  }
  }
  return (

    <div className='card' onClick={deleteData}>
      <div className='card-inner'>
        <div className='card-front'>
          <img src={item.img} alt='' />
        </div>
        <div className='card-back'>
          <h1>{item.name}</h1>
          <ul>
            <li>
              <strong>Deskripsi:</strong> {item.description}
            </li>
            <li>
              <strong>Penulis:</strong> {item.author}
            </li>
            <li>
              <strong>Penerbit:</strong> {item.publisher}
            </li>
            <li>
              <strong>Tahun terbit:</strong> {item.yearofissue}
            </li>
            <li>
              <strong>Harga:</strong> <span>Rp.</span> {item.price}
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}

export default CharacterItem
