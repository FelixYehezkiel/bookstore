-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 16 Jul 2020 pada 06.09
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookstore`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(37);

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_master`
--

CREATE TABLE `product_master` (
  `id` int(11) NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `img` text DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `yearofissue` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `product_master`
--

INSERT INTO `product_master` (`id`, `author`, `description`, `img`, `name`, `price`, `publisher`, `yearofissue`) VALUES
(28, 'felix', 'bagus', 'https://m.media-amazon.com/images/M/MV5BMGE0OWUyMTktNjNjNi00ZTg0LWE1N2MtMzEzYjdhNjM5M2M5XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_.jpg', 'secret', '5000', 'paramadaksa', '2020'),
(31, 'Joseph Wijaya', 'Ini buatanku', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxALChAQEBAJEBAJDQoNCAkJBw8ICQcKIB0iIiAdHx8kKDQsJCYxJx8fLTItMT1AQzBDIys9QD8uQDQuRDcBCgoKDQ0NEg0NDisZFSU3NzcrLSs3KystNCstKystKy0rLS0rKystKysrKystKysrKys3KzcrKysrKysrKy0tK//AABEIAMgAyAMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAAAgEDBQQGB//EAD0QAAIBAgMFBAYIBQUBAAAAAAABAgMRBBIhBTFBUWEGEyJxMoGRobHwI0JSYnLB0fEUJDNT4TRDgpKTJf/EABgBAQEBAQEAAAAAAAAAAAAAAAABAgME/8QAIREBAQACAwACAgMAAAAAAAAAAAECEQMhMRJBE4EiUWH/2gAMAwEAAhEDEQA/APUgKLc6aEyGSIsOENFfPMawsGMgqVG5E4FsCJvUgoVPUsVP5sSXRAqVHUujTS/YOI19CaNnhTuS6I1NjyVyKrVIdUfmw6QyKivuUMqCGGiwhe4Qdwi24uYKVUUT3SGTuBdCO6QypIlMYBO6XyySwCJt5YlEEmlSSyAIhojxFgOkUMmS9RUWJEFTLKbCUb/mLUmqUHJ7oq7sm2VVqevxHsfP9qdt6neONCMIxTsqk4qcpGY+12Mv/Vf/AJwt8BpNx9YgWZj5Zhu22LhJZpU5rjGdKKv7D1uwu1tLGSUJLu6ktI+LNTnImjb1CGK4jxIqSYkDxRUNfQQZohIi1KRKCwyRYBE2IGQQJgFgJ0jzSiTkLEgsaaVZRmh8pKiVP2WMSyKCMR4ogVKw6RIAFjynbfbsaFJ0IO9SqrVXFtOhH/J6bGVu5ozn/bhKW658ZxuJlXqyqSbcqjbk3d6iJa527/n1JimdOEw2d3e73G1hMHBrcvO1rkyzkaw47k8+ot8PcXUpOLTWjVra2aZ6yls+CS0j7FoLi9lwlBpKKfBxSTuY/J/jp+Cz7en7HdoP42n3dSyq0orW/wDXjz8z058e7P1JYfaNJ3acakYy1t4dx9iSNuQSHigiixIgWwZR0h0gbVWAtcRbWKFt82JSJJSIiLAWJANGnm0SkGUncbWpsTYRyHTAkmJCJRA0ScoRJCKsTQVSlOD3VISi/WfE6sHGbi98W0+jPuSPlfaLZ6hjZ5dFKvWTV7pajw+NrkwNN2SSbb9SRtYfD1Y2bjSa4qMm5FWGou1o6N7nbVHZSoSoxk3OcrtNZptqC5HC3b14Y600MiyJpN6einbUoVOtN6RopclUcmW4Gq6lFrTV6PiRg8BKEpN1Kl204rO3l95l0rHWCb2pTik1306UrNap31Pq8Ty+z8OpY+hNpXiqyWnG3+D1aidsbuPJnNZCI6ISHRWKEh7AkMkU8RFA0MkK95AjiTFDJEpAKA+XQC7Hm5MRstYkjQEhkiESESkMkKkOkRUxGFykj9ILniO1uCdPFKf1ajcou2mbie3S1Ofa+CjicNOLSuoylSlbWE0S9tY3TxOzt/U69oQeVLhNq8mtInDgpZZHTjqzUU7Npvg9xwvr2Y3p17NcYUl4amsklOydutjSw8Ly14N8N5kYJvKnZPl4tLGvhZuUbtZXwjdOxmttPZlK+IUuFNSa6M3InFsullp3a1nZv8J3pHfGajxcmW6LDRRKQyRpgJkphbUkiAmxMUPYgraBIssEYhSNaAW2IKjyrfzzIkySJGwKQyYlh4xIHiWRQkC6CCocRSyxEkkruy5u9kgEM3tHi+5wjUW1OveEHF2lCPF/PMsxO1KdO6Tc2vq09Y38zyG2NoVK2Li6iShUjkoQV1GnL/P5I6Y4W3xi1l0asqcsst/1ZW0kjRp1c6SfDcWTwKnHVeTtrBlUdnzjutpwvdNGObi+Pb0cXJvps4RRVnd7tVm8Jq7Ood9Vt9WGsrcTzmDoyzWeb/tdHotkYxUcR3T0jUjC8/sT1scMMflk7cmWsXo6at+Vi6IkUWwOrxnQyRER1FkomMCcgyJAhIkmxNgFAmxOUCEAyiBNmnkiJbiSGrnQCJRA8EQiYby+DK0jP2rtJUPBFp1KlkuKpR5lk30bX7S2rTwyt6U3upxfo+ZiTrVcW7zdo/VpxbUV6jPpJ1KuaTupT1beZydzZpU9XyvdcrHpx45O6xaqpYaNrcdbfeRxbU2e69JxVlKLzUXeyhI1svi03x9JBKKautztbidGWLgq3fUVJq0leNaG506q0aL4VlTV5bo34XbYkcFOOMm4p93iVnqPS1OsrL3/AJHJOMqWP7qvbJiP9FVtljCXL56DLVmrFm53K3MDVp1Y5oqP3la0oMy8PXdSvUbVr1JqPG0VovgdUcJOhPMtV9ZXtmiUxpONOU2m1Tdao3BLM4b/AGnnw4fhnueO2XL8sdVv4HaNSM0r3TjLwTd3CSNzB42NSyekmlZfVkeRwsZ/R1VlakrqMGvDHk+ZqKLtpo1rDW7TN5ccrlvT1MVqXJGXsfG9/Bp+nTtm5yRqI8tmrqtpsMkESUZ2SbCQfO4kaP7BfEJEDsWwBEBooBEteLzE3ET/AMBc6CwaLKrjkFWOxioUZT5eiucjykJupVcpO7kpSb+8zZ7Sf0Y/jV+W4yMNDwyf4UvM78UZydOFjajm+xV104Ghs9+nB/7UvDfXwcDmwsL4SXSbZ00NK9/7lGL/AOS/c7snnPLXXKSs+o8Flm48NHHTgc2Nl9JHzXsOySuoy+zv/CESo2fwOXa2zli6Dg3aUfFRqcaczukrP4DxJexlbBxrr0XCppWwr7vExbu5S5/PJmnPDwyKylmqZo193dyi93z5mLtOH8Hj6eJWlPEZaOM4KPX3e7qehjHQyrN2TT7pVKL34aacNb3pSV1+Zpyj4b8UcC02jNf3cPCb807fmatt3kAuzHkxKa3VNJLgj00VqeawitUS6qx6WBw5vW8TpDKJKGRwW1FiCxIhxB2VILDWCwTtCQDIkbX4vAXBsUDqGzlsJXRzsenIgz+0NW1OMeMpX8kv3M/CQvSn+KPtsNturnxFuFOMUvPePgFehL8b+CPRxzpmunZzzUJrk7svorxU3yVWL8tDl2ZK1SUftxdvM6l4cvSbj7U/0OrKvFQzVo8nrfcXYKrmUov6r8PVEYqyV+MczRy4GTadufrsEay9FdNGTAow08zad+HHQ6IwS4e7Ugrx2GjiMPOm7fSRkk9+WXBlHZ2u6mFUZenh26VRcbrd7jQh+xmQaw+0JLhi4xla9k5fvf2ma0Zx/wDq354VN8rZjZjqjNxVW+0aWkU+5qp5Va8dP0NKCCGpq00/Js9FDd52sefWuns6G9hHenF9Les4c301i6IodIVDo4VqBIlokApQJsFiAsBIF0PndyHMVOyIOiHb0FjKxAoGJtF3xMvNfA7tlq+Hl+OXwRxY9fzEvKPDjY0dkr+Xf45Hpw8Yqqi8tZPrqdtaGjtrlcZRX2vm5yVo2mn1XA7sRPLTz6+GLvz5nRGV/EydNZnd2Tb+0y7CeGK+9e3mckLztwStdcDtpuy8rW0LUdNB5ai5PeaJlYdpTV93DkatPh6jNU8fnUyu01JqhGtFeLDSTbT1jB8fbY1k/m5FWkqtOUJaxqRlGSvrlZmqycLWVfH0aitaeHlJ66J2/U3onlOydKUcZOk1JvCwrx0TdlmWvxPWtE2Gp+kjV2VUvmi+eaPkZK09x2YGeSvD714vqY5JuEbsR0hYoe55WwAXBFAAAAAAAfNmBFyGdBNwIuLmuNjH2hK9eX/H4GnsVp0bfek/MyMRK9Sb+9LXoaezHloxa5yv1PTh4xV2Mp2Gq3ng5r7k/VY6asFOPq5GVtKrOlg55eEoxq840no/yNsuWhJ+zodspXOTDpTgpL1l0Xz6GlXxd2vebFHcuqRkZbLTju0uamDd6afJWdmnZmKOpDJa/O8RP54jsyMTBVO523WSbTrUYqLindr5R6KD+eRkzpJ7Qi3JxtQlKnpmjWknZr33v+pqJXJ/arJL37h6Lfext9Vr2lcJcH+w9CeSsn9lrTciXyj1CJQqd/yJPG6JAAKgAAAAJICvmjI8yQNojgV348rlrQskopt7km35CDz17+138zawFv4VdJS4bzGgr+u9lfcbOy1fDtcpysevFiuzCydnva96ZTtKClhqttPoqjas7XSuvgXYPRtC7W0w9TrCUejT0KjCwD7uVuEkr8kzRnSW9bvM41T4+zod2GldW/c0Epzto/V0NbZ8bQfX2WMytStqvWduyJ3UlxVreRmjuvqcm19p/wAKqfgzutJxj9IqcYux2W1MPtevoKb+zWWvLRmfocWN2hiKlanNUYR7iV4rvlVc9edtDSwu267r01OGHgqlSnFxTlOo03bmd2xtk06mEhUkqkpTgpS/mJU4R9iOHtBhI4eVGUYQpuFWlmyznOU1vu7vocvn2bj0c42Yub6TzylzV439pzpvvbdE2zoPU4OealHokn5l5n7JleDXJqxoI8eU1dOkAAAAAAAAAAfNQADoASpDNFrmmgADFq03RlaXHWOt7o0dlSWRrnK61AD043pitJR4lO2P9LJ9aSl1WZEgaRnyjZ257uhNGWWfR6PkgAo6peF67nuDBvu66tune3IAINixidql/KrpVpfoAGaNbYza2RSnUnJ05pwp0bJpT7zd7mcPa2bqYSm4QqKnRcUpyX+5ua5cAA8+u1+noaeq872Oac7VEuLSdr2bVwA7xGvseulOz0zLw6q1zaRAHm5Z/J0nh4sGgA5qUAAqAAAK/9k=', 'Kak Ocep Story', '50000', 'Paramadaksa', '2020'),
(33, 'Samuel M', 'Ini buku buatan samuel', 'https://media.suara.com/pictures/970x544/2019/09/16/17190-timnas-indonesia-u-16.jpg', 'Samuel Story', '5000000', 'Paramadaksa', '2020'),
(34, 'Kak Wawan Setiawan', 'Ini awal munculnya token PLN', 'https://i1.wp.com/klikall.com/blog/wp-content/uploads/2018/11/cara-isi-token-listrik.png?fit=800%2C600&ssl=1&w=640', 'Sejarah token PLN', '500000', 'Paramadaksa', '2020'),
(35, 'Kak wawan', 'Yang buat kak wawan', 'https://asset.kompas.com/crops/ItYXBl0Xx9FgkdZw2b-lBfJDqXU=/124x0:1202x719/750x500/data/photo/2019/08/17/5d57631fdc8db.jpg', 'Sejaran token listrik', '500000', 'Paramadaksa', '2020');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `product_master`
--
ALTER TABLE `product_master`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
